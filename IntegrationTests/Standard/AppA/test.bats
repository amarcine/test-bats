# Demonstration of a successful test which hides the output of the app from the
# reports.


# NOTE: better than shellspec because at least no need to hard-code the actual path
cd $BATS_TEST_DIRNAME

@test 'runs' {
  run ./appA.sh

  [ "$status" -eq 0 ]
  [[ "$output" == *message* ]]
}
