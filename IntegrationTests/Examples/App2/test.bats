# Demonstration of a failing test


# NOTE: better than shellspec because at least no need to hard-code the actual path
cd $BATS_TEST_DIRNAME

@test 'runs' {
  run ./app2.sh

  [ "$status" -eq 0 ]

  # NOTE: on failure I would like to get the full, raw output in the report
}
