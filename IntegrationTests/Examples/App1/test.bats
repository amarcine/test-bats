# Demonstration of several checks in different contexts


# NOTE: better than shellspec because at least no need to hard-code the actual path
cd $BATS_TEST_DIRNAME

@test 'prints message without args' {
  run ./app1.sh

  [ "$status" -eq 0 ]
  [ "$output" = "message" ]
}

@test 'fails if given args' {
  run ./app1.sh something

  [ "$status" -eq 1 ]
}
